/* 
 Теоретичні питання
 1. Як можна сторити функцію та як ми можемо її викликати? 
 2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
 3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
 4. Як передати функцію аргументом в іншу функцію? 

 Практичні завдання
 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
 
 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

Технічні вимоги:
- Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
- Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
- Створити функцію, в яку передати два значення та операцію.
- Вивести у консоль результат виконання функції.

3. Опціонально. Завдання: 
Реалізувати функцію підрахунку факторіалу числа. 
Технічні вимоги:
- Отримати за допомогою модального вікна браузера число, яке введе користувач.
- За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
- Використовувати синтаксис ES6 для роботи зі змінними та функціями.
*/

// Відповіді:
// 1. Функцію можна викликати звернувшися до неї по оголошеній привязці (назві) та передав за необхідності відповідні аргументи для опрацювання.

//     Function declaration
//     Function declaration (Оголошення функцій) визначає ім'я функції за допомогою ключового слова `function`.
//     Зазвичай вони піднімаються (hoisted), що означає, що вони доступні в усій області,
//     в якій вони визначені, навіть якщо вони оголошені після їх використання.

//     Function Expression
//     Function Expression передбачає визначення функції та присвоювання її змінній. Ці функції можуть бути анонімними або мати ім'я.
//     Вони не піднімаються (не доступні до виклику) і можуть бути використані лише після того, як їх призначено.

// 2. Оператор повернення(return):
// Використовується для завершення виконання функції та вказує значення, яке повертається викликачеві.
// Оператори повернення дозволяють функціям передавати дані коду, що їх викликав, сприяючи передачі обчислених значень,
// результатів або будь-якої іншої інформації, необхідної для подальшої обробки чи маніпуляцій. Розуміння операторів
// повернення є фундаментальним у JavaScript, оскільки вони впливають на поведінку та результати функцій.

// 3.
//     Параметри Функцій:
//     Параметри - це місцедержателі, вказані в оголошенні функції. Вони представляють значення, які функція очікує отримати під час виклику.
//     Оголошення Функції: Параметри визначаються всередині дужок оголошення функції і діють як змінні всередині функції.
//     Значення-замінники: Параметри діють як локальні змінні всередині функції, представляючи значення, які передаються функції при її виклику.

//     Аргументи Функцій:
//     Аргументи - це фактичні значення, що подаються до функції при її виклику. Вони замінюють параметри всередині функції.
//     Виклик Функції: Аргументи вказуються всередині дужок під час виклику функції, у відповідності до порядку параметрів функції.
//     Гнучкість Аргументів: Функції можуть бути викликані з будь-якою кількістю аргументів, незалежно від кількості параметрів,
//     вказаних у визначенні функції. Додаткові аргументи ігноруються, а відсутні аргументи отримують значення `undefined`.
//     Іноді терміни "аргумент" і "параметр" можуть використовуватися взаємозамінно. Проте, за визначенням, параметри - це те,
//     що ви вказуєте в оголошенні функції, у той час як аргументи - це те, що ви передаєте у функцію.

// 4. Функцію, як аргумент можна передавати в іншу функцію без додавання синтаксичних змін, лише по оголошеній привязці (назві функції). Потім
// ця фунція використовується всередини блоку іншої функції як зазвичай, з передачею аргументів та викликом її за необхідності.

//  Практичні завдання
//  1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

// function divisionResult(divisible, divisor) {
// 	if (divisible === 0 && divisor === 0) {
// 		return 'Numbers are zero. Division is undefined';
// 	} else if (divisor === 0) {
// 		return 'Division by zero is not allowed';
// 	} else {
// 		return divisible / divisor;
// 	}
// }
// console.log(divisionResult(10, 0));

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

// let firstNumber = prompt('please, enter first number');
// while (
// 	firstNumber === null ||
// 	firstNumber === '' ||
// 	isNaN(Number(firstNumber))
// ) {
// 	firstNumber = prompt('Invalid value. Please, enter a valid first number');
// }
// firstNumber = Number(firstNumber);

// let secondNumber = prompt('please, enter second number');
// while (
// 	secondNumber === null ||
// 	secondNumber === '' ||
// 	isNaN(Number(secondNumber))
// ) {
// 	secondNumber = prompt('Invalid value. Please, enter a valid second number');
// }
// secondNumber = Number(secondNumber);

// let mathOperation = prompt(
// 	'What math operation do you want to do. Please enterchoose one of the proposed ones: +, -, *, /.'
// );

// function mathResult(num1, num2, operation) {
// 	if (num2 === 0 && operation === '/') {
// 		return 'Division by zero is not allowed';
// 	}
// 	switch (operation) {
// 		case '+':
// 			return num1 + num2;
// 		case '-':
// 			return num1 - num2;
// 		case '*':
// 			return num1 * num2;
// 		case '/':
// 			return num1 / num2;
// 		default:
// 			return 'Invalid operation';
// 	}
// }

// console.log(mathResult(firstNumber, secondNumber, mathOperation));

// 3. Опціонально. Завдання:
// Реалізувати функцію підрахунку факторіалу числа.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.

let numberForFactorial = prompt('please, enter first number');
while (
	numberForFactorial === null ||
	numberForFactorial === '' ||
	isNaN(Number(numberForFactorial))
) {
	numberForFactorial = prompt(
		'Invalid value. Please, enter a valid first number'
	);
}
numberForFactorial = Number(numberForFactorial);

function factorial(n) {
	let count = 1;
	if (n === 0 || n === 1) {
		return 1;
	} else {
		for (let i = 1; i <= n; i++) {
			count = count * i;
		}
		return count;
	}
}
console.log(
	`factorial of nubmer ${numberForFactorial} is ${factorial(
		numberForFactorial
	)}`
);

function factorialRecursion(n) {
	if (n === 0 || n === 1) {
		return 1;
	}
	return n * factorialRecursion(n - 1);
}

console.log(`factorial of nubmer ${numberForFactorial} is ${factorialRecursion(numberForFactorial)}`);


